package org.pbrane.mike.tictactoe;

import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Point;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

// the tic-tac-toe game using minimax with alpha beta pruning AI

class TicTacToe {
	private static final int X_SCORE = 0;
	private static final int O_SCORE = 1;

	final private static int LINE_WIDTH = 30;
	final private static int GAME_COLOR = 0xffc5c5c5;
	final private static int WINNER_COLOR = 0xffcd0000;

	private final int nCells = 9;
	private final Canvas canvas;
	private final Point size;
	private final int cell_size;
	private int currentCell;
	private int currentPlayer;
	private final int[] board;
	private final List<Point> layout = new ArrayList<>(nCells);
	private int theWinner;
	private final int[] winning;
	private boolean AIsTurn;
	private int nodeLevel;
	private final int[] score;

	// These are the 8 possible 3-in-a-row winning patterns...
	private final int[][] pattern = {
			{0, 1, 2},
			{3, 4, 5},
			{6, 7, 8},
			{0, 3, 6},
			{1, 4, 7},
			{2, 5, 8},
			{2, 4, 6},
			{0, 4, 8}
	};

	public enum cellType {
		INVALID_CELL(-3),
		TTT_IN_PLAY(-2),
		TTT_TIE(-1),
		TTT_EMPTY(0),
		TTT_X(1),
		TTT_O(2);

		cellType(int i) {
			this.value = i;
		}

		public int value;
	}

	TicTacToe(Canvas canvas, Point size) {
		this.canvas = canvas;
		this.size = size;
		cell_size = size.x / 3;
		currentCell = cellType.INVALID_CELL.value;
		currentPlayer = AIsTurn ? cellType.TTT_O.value : cellType.TTT_X.value;

		board = new int[nCells];
		Arrays.fill(board, cellType.TTT_EMPTY.value);
		Arrays.fill(board, 0);

		score = new int[2]; // X and O score

		theWinner = cellType.TTT_TIE.value;
		winning = new int[3];

		layout.add(new Point(0, 0));
		layout.add(new Point(cell_size, 0));
		layout.add(new Point(cell_size * 2, 0));
		layout.add(new Point(0, cell_size));
		layout.add(new Point(cell_size, cell_size));
		layout.add(new Point(cell_size * 2, cell_size));
		layout.add(new Point(0, cell_size * 2));
		layout.add(new Point(cell_size, cell_size * 2));
		layout.add(new Point(cell_size * 2, cell_size * 2));
	}

	void toggleCurrentPlayer() {
		currentPlayer = (currentPlayer == cellType.TTT_O.value) ? cellType.TTT_X.value : cellType.TTT_O.value;
	}

	// AI player is always 'O'
	void setAIPlayer() {
		currentPlayer = cellType.TTT_O.value;
	}

	int getNodeLevel() {
		return nodeLevel;
	}

	String getCurrentPlayerSymbol() {
		return currentPlayer == cellType.TTT_X.value ? "X" : "O";
	}


	int[] getScore() {
		return score;
	}

	void resetGame() {
		Arrays.fill(board, cellType.TTT_EMPTY.value);
		theWinner = cellType.TTT_TIE.value;
		currentCell = cellType.INVALID_CELL.value;
		currentPlayer = AIsTurn ? cellType.TTT_O.value : cellType.TTT_X.value;
		nodeLevel = 0;
	}

	void drawBoard() {
		Paint paint = new Paint();
		paint.setColor(GAME_COLOR);
		paint.setStrokeWidth(LINE_WIDTH);
		paint.setStrokeCap(Paint.Cap.ROUND);
		paint.setStyle(Paint.Style.FILL_AND_STROKE);
		float[] pts = new float[16];
		final float MARGIN = 16.0f;

		// first vertical line on left
		pts[0] = cell_size;
		pts[1] = MARGIN;
		pts[2] = cell_size;
		pts[3] = (cell_size * 3.0f) - MARGIN;
		// second vertical line on right
		pts[4] = cell_size * 2.0f;
		pts[5] = MARGIN;
		pts[6] = cell_size * 2.0f;
		pts[7] = (cell_size * 3.0f) - MARGIN;
		// first horizontal line on top
		pts[8] = MARGIN;
		pts[9] = cell_size;
		pts[10] = size.x - MARGIN;
		pts[11] = cell_size;
		// second horizontal line on bottom
		pts[12] = MARGIN;
		pts[13] = cell_size * 2.0f;
		pts[14] = size.x - MARGIN;
		pts[15] = cell_size * 2.0f;

		canvas.drawLines(pts, paint);
	}

	int findCell(int x, int y) {
		int cell;

		cell = x / cell_size;
		cell += ((y / cell_size) * 3);

		return cell;
	}

	void setCurrentCell(int cell) {
		currentCell = cell;
	}

	int getCurrentCell() { return currentCell; }

	boolean drawPlayer() {
		if (currentPlayer == cellType.TTT_X.value) {
			drawX(GAME_COLOR, layout.get(currentCell).x, layout.get(currentCell).y);
		} else {
			drawO(GAME_COLOR, layout.get(currentCell).x, layout.get(currentCell).y);
		}
		return true;
	}

	private void drawX(int color, int x, int y) {
		int cell = findCell(x, y);

		Paint paint = new Paint();
		paint.setColor(color);
		paint.setStrokeWidth(LINE_WIDTH);
		paint.setStyle(Paint.Style.STROKE);
		paint.setStrokeCap(Paint.Cap.ROUND);
		paint.setStyle(Paint.Style.FILL_AND_STROKE);

		float[] segments = new float[8];
		float offset = cell_size * 0.1f;

		segments[0] = x + offset;
		segments[1] = y + offset;
		segments[2] = x + (cell_size - offset);
		segments[3] = y + (cell_size - offset);

		segments[4] = x + (cell_size - offset);
		segments[5] = y + offset;
		segments[6] = x + offset;
		segments[7] = y + (cell_size - offset);

		canvas.drawLines(segments, paint);
		board[cell] = cellType.TTT_X.value;
	}

	private void drawO(int color, int x, int y) {
		int cell = findCell(x, y);

		Paint paint = new Paint();
		paint.setColor(color);
		paint.setStrokeWidth(LINE_WIDTH);
		paint.setStyle(Paint.Style.STROKE);
		int offset = cell_size / 2; // the center of the cell

		canvas.drawCircle(x + offset, y + offset, cell_size / 2.5f, paint);
		board[cell] = cellType.TTT_O.value;
	}

	boolean checkForWinner() {
		for (int[] aPattern : pattern) {
			if (board[aPattern[0]] == board[aPattern[1]] && board[aPattern[1]] == board[aPattern[2]]) {
				if (board[aPattern[0]] != cellType.TTT_EMPTY.value) {
					theWinner = board[aPattern[0]];
					if (theWinner == cellType.TTT_X.value) {
						score[X_SCORE]++;
					} else {
						score[O_SCORE]++;
					}
					winning[0] = aPattern[0];
					winning[1] = aPattern[1];
					winning[2] = aPattern[2];
					return true;
				}
			}
		}
		return false;
	}

	private int getTheWinner() {
		return theWinner;
	}

	boolean isAIsTurn() {
		return AIsTurn;
	}

	void setAIsTurn(boolean value) {
		AIsTurn = value;
	}

	boolean hasMovesLeft() {
		for (int cell = 0; cell < nCells; cell++) {
			if (board[cell] == cellType.TTT_EMPTY.value) {
				return true;
			}
		}
		return false;
	}

	boolean cellIsEmpty(int cell) {
		return board[cell] == cellType.TTT_EMPTY.value;
	}

	String getTheWinnersName() {
		return theWinner == cellType.TTT_X.value ? "Meatbag" : "AlIce";
	}

	void AIsMove() {
		if (AIsTurn && !checkForWinner() && hasMovesLeft()) {
			currentCell = findBestMove(board);
			drawPlayer();
		}
	}

	private int findBestMove(int[] board) {
		int bestVal = Integer.MIN_VALUE;
		int bestMove = cellType.INVALID_CELL.value;
		int depth = 0;
		int alpha = Integer.MIN_VALUE;
		int beta = Integer.MAX_VALUE;
		nodeLevel = 0;

		for (int cell = 0; cell < nCells; cell++) {
			if (board[cell] == cellType.TTT_EMPTY.value) {
				board[cell] = cellType.TTT_O.value;
				int moveVal = miniMaxAB(board, depth, alpha, beta, true);
				board[cell] = cellType.TTT_EMPTY.value;
				if (moveVal > bestVal) {
					bestMove = cell;
					bestVal = moveVal;
				}
			}
		}
		return bestMove;
	}

	private int miniMaxAB(int[] board, int depth, int alpha, int beta, boolean isHumansMove) {
		nodeLevel++;
		switch (evaluate(board)) {
			case 0: // player has won
				return -1;
			case 1: // computer has won
				return 1;
			case -1: // its a tie game
				return 0;
		}
		if (isHumansMove) { // humans move
			int best = Integer.MAX_VALUE;
			for (int cell = 0; cell < nCells; cell++) {
				if (board[cell] == cellType.TTT_EMPTY.value) {
					board[cell] = cellType.TTT_X.value;
					best = Math.min(best, miniMaxAB(board, depth++, alpha, beta, false));
					board[cell] = cellType.TTT_EMPTY.value;
					beta = Math.max(beta, best);
					if (beta <= alpha) {
						break;
					}
				}
			}
			return best;
		} else { // AI's move
			int best = Integer.MIN_VALUE;
			for (int cell = 0; cell < nCells; cell++) {
				if (board[cell] == cellType.TTT_EMPTY.value) {
					board[cell] = cellType.TTT_O.value;
					best = Math.max(best, miniMaxAB(board, depth++, alpha, beta, true));
					board[cell] = cellType.TTT_EMPTY.value;
					beta = Math.max(alpha, best);
					if (beta <= alpha) {
						break;
					}
				}
			}
			return best;
		}
	}

	private int evaluate(int[] board) {
		for (int[] aPattern : pattern) {
			if (board[aPattern[0]] == board[aPattern[1]] && board[aPattern[1]] == board[aPattern[2]]) {
				if (board[aPattern[0]] == cellType.TTT_X.value) {
					return 0;
				} else if (board[aPattern[0]] == cellType.TTT_O.value) {
					return 1;
				}
			}
		}
		return (hasMovesLeft() ? cellType.TTT_IN_PLAY.value : cellType.TTT_TIE.value);
	}

	// draw the winning move in red
	void showWinner() {
		for (int cell = 0; cell < 3; cell++) {
			if (getTheWinner() == cellType.TTT_X.value) {
				drawX(WINNER_COLOR, layout.get(winning[cell]).x, layout.get(winning[cell]).y);
			} else {
				drawO(WINNER_COLOR, layout.get(winning[cell]).x, layout.get(winning[cell]).y);
			}
		}
	}

}
