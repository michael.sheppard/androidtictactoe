package org.pbrane.mike.tictactoe;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Point;
import android.graphics.Typeface;
import android.os.Bundle;
import android.util.TypedValue;
import android.view.Display;
import android.view.Gravity;
import android.view.MotionEvent;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import java.util.Locale;


public class MainActivity extends AppCompatActivity {
	final private static int NO_CELL = -1;
	private GameView tictactoeView;
	private Canvas canvas;
	private TicTacToe ttt;
	private TextView textView;

	private boolean singlePlayerMode;
	private boolean firstMove = false;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);

		tictactoeView = findViewById(R.id.GameView);
		Point size = new Point();
		Display display = getWindowManager().getDefaultDisplay();
		display.getSize(size);
		// the tic-tac-toe board is square, hence the size.x dimensions
		int dims = size.x;
		Bitmap bitmap = Bitmap.createBitmap(dims, dims, Bitmap.Config.ARGB_8888);
		canvas = new Canvas(bitmap);
		tictactoeView.setImageBitmap(bitmap);

		textView = findViewById(R.id.text_view);
		textView.setTextColor(0xffc5c5c5);
		textView.setTypeface(Typeface.MONOSPACE);
		textView.setTextSize(TypedValue.COMPLEX_UNIT_SP, 14.0f);

		singlePlayerMode = true; // default to playing the AI

		ttt = new TicTacToe(canvas, size);
		ttt.drawBoard();
		displayInGameMessage(NO_CELL, ttt.getCurrentPlayerSymbol(), ttt.getNodeLevel(), singlePlayerMode);

		// this method happens when it is the players turn
		tictactoeView.setOnTouchListener((view, event) -> {
			// check for winner or game over or AI's turn
			if (ttt.checkForWinner() || !ttt.hasMovesLeft() || ttt.isAIsTurn()) {
				return false;
			}

			view.performClick();

			if (event.getAction() == MotionEvent.ACTION_UP) {
				return updateGameState((int) event.getX(), (int) event.getY());
			}
			return true;
		});
	}

	private void notifyPlayer(String message) {
		Toast toast = Toast.makeText(getApplicationContext(), message, Toast.LENGTH_LONG);
		toast.setGravity(Gravity.CENTER_VERTICAL, 0, 0);
		toast.show();
	}

	public void onResetGame(View view) {
		canvas.drawColor(Color.BLACK);
		firstMove = false;
		ttt.resetGame();
		ttt.drawBoard();
		displayInGameMessage(NO_CELL, ttt.getCurrentPlayerSymbol(), ttt.getNodeLevel(), singlePlayerMode);
		tictactoeView.postInvalidate();
	}

	public void onChangePlayMode(View view) {
		singlePlayerMode = !singlePlayerMode;
		displayInGameMessage(-NO_CELL, ttt.getCurrentPlayerSymbol(), ttt.getNodeLevel(), singlePlayerMode);
		tictactoeView.postInvalidate();
	}

	public void onAIFirstMove(View view) {
		if (!firstMove) {
			// AI makes the first move
			if (singlePlayerMode) {
				firstMove = true;
				ttt.setAIsTurn(true);
				// AI is always 'O'
				ttt.setAIPlayer();
				AIsTurn();
				if (ttt.checkForWinner()) {
					ttt.showWinner();
					// display message about the winner
					String winner = String.format("%s has won the game!", ttt.getTheWinnersName());
					notifyPlayer(winner);
					tictactoeView.postInvalidate();
				} else {
					ttt.toggleCurrentPlayer();
				}
			}
			// check if this was the last move, otherwise keep playing
			if (!ttt.hasMovesLeft()) {
				// it's a tie game, display message about the two losers
				notifyPlayer("Stalemate!");
				tictactoeView.postInvalidate();
			}
			displayInGameMessage(ttt.getCurrentCell(), ttt.getCurrentPlayerSymbol(), ttt.getNodeLevel(), singlePlayerMode);
			tictactoeView.postInvalidate();
		}
	}

	private boolean updateGameState(int x, int y) {
		int cell = ttt.findCell(x, y);
		// check if cell is empty
		if (!ttt.cellIsEmpty(cell)) {
			return false;
		}
		// draw the current player symbol
		ttt.setCurrentCell(cell);
		if (ttt.drawPlayer()) {
			ttt.toggleCurrentPlayer();
		}
		// check for winner after this move
		if (ttt.checkForWinner()) {
			ttt.showWinner();
			// display message about the winner
			String winner = String.format("%s has won the game!", ttt.getTheWinnersName());
			notifyPlayer(winner);

			tictactoeView.postInvalidate();
			return true;
		}
		// toggle player to the computer, and call method
		if (singlePlayerMode) {
			ttt.setAIsTurn(true);
			AIsTurn();
			if (ttt.checkForWinner()) {
				ttt.showWinner();
				// display message about the winner
				String winner = String.format("%s has won the game!", ttt.getTheWinnersName());
				notifyPlayer(winner);
				tictactoeView.postInvalidate();
				return true;
			} else {
				ttt.toggleCurrentPlayer();
			}
		}
		// check if this was the last move, otherwise keep playing
		if (!ttt.hasMovesLeft()) {
			// it's a tie game, display message about the two losers
			notifyPlayer("Stalemate!");
			tictactoeView.postInvalidate();
			return true;
		}
		displayInGameMessage(cell, ttt.getCurrentPlayerSymbol(), ttt.getNodeLevel(), singlePlayerMode);
		tictactoeView.postInvalidate();
		return true;
	}

	private void AIsTurn() {
		ttt.AIsMove();
		ttt.setAIsTurn(false);
	}

	private void displayInGameMessage(int cell, String symbol, int node, boolean singlePlayer) {
		textView.setText("");
		textView.append(String.format(Locale.US, "Player %s's turn\n", symbol));
		textView.append(String.format(Locale.US, "Current Cell: %d\n", cell));
		textView.append(String.format(Locale.US, "Scores:  X: %d   O: %d\n", ttt.getScore()[0], ttt.getScore()[1]));
		textView.append(String.format(Locale.US, "Two player mode is %s\n", singlePlayer ? "disabled" : "enabled"));
		textView.append(String.format(Locale.US, "Nodes traversed: %d\n", node));
		textView.append(String.format(Locale.US, "\n%s\n", getVersionString()));
	}

	String getVersionString() {
		PackageInfo pacInfo;
		String version = "";
		String build = "";

		Context context = getApplicationContext();
		if (context != null) {
			final PackageManager packageManager = context.getPackageManager();
			if (packageManager != null) {
				try {
					pacInfo = packageManager.getPackageInfo(context.getPackageName(), 0);
					String[] tmp = pacInfo.versionName.split("-");
					version = tmp[0];
					build = tmp.length > 1 ? tmp[1] : "";
				} catch (PackageManager.NameNotFoundException e) {
					e.printStackTrace();
					version = "kr4p";
				}
			}
		}
		if (build.equalsIgnoreCase("dirty")) {
			return version + "." + "dev";
		} else {
			String padding = "0000"; // 4 digits in build number
			String result = padding + build;
			return version + "." + result.substring(result.length() - padding.length());
		}
	}

}